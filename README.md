# Introduction

Ces ateliers ont pour objectif de s'entre-aider et de découvrir des techniques pouvant être utile dans notre pratique.

# Prochains ateliers

## 2018-11-27 : Organiser un tableau de données

Comment organiser ses données ?

Animateur : Thibaut(s)

Ressource : instruction ShinyStat ou autre

Lieu : DIM ?

## 2018-11-21 : Gestion de version avec Git 1

### Prérequis :

- [ ] avoir vu la vidéo du MOOC sur GIT
- [ ] GIT installé
- [ ] RStudio installé
- [ ] PC avec connexion internet
- [ ] Si possible, avoir créé son compte GitLab

### Programme

- Questions/Réponse
- Test de Git avec un GUI : Rstudio
- Pousser vers GitLab

# Ateliers passés

## 2018-11-21 - Pandoc et l'interface en ligne de commande

#### Ressources

Voici un exemple d'usage intéressant de markdown et pandoc à savoir la création d'un CV en différents formats :
https://mszep.github.io/pandoc_resume/

### Notes

Faire plus simple

## 2018-11-31 : markdown

## 2018-10-24 : Introduction à l'analyse réplicable





# Idées pour de prochains atelier

- Formater une base de données simple dans un tableur (animation : Thibaut Fabacher)
- R : comment choisir son package
- R programmation fonctionnelle (by : Lise)
- Logiciel libre