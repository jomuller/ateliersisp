# Atelier 4

Thème : Git

Objectif : Comprendre le principe général des systèmes de gestion de version et des plateformes de gestion de version collaborative

# Déroulé

1. Tour de table : 1 info et 1 question sur le thème par personne
2. Démonstration de l'intérêt de Git
  - Exemple : [inkscape](https://gitlab.com/inkscape/inkscape), un logiciel d'édition de graphismes vectoriels
  - Explorer
  - Voir la vitalité d'un projet
  - Participer
3. Manipulations :
  - Wikipedia
  - GitLab
  - Rstudio
4. Conclusion : définition du thème suivant

# Supports

- MOOC recherche reproductible, module 1 session 4
- [Pro Git](https://git-scm.com/book/fr/v2)
- [GitLab.com](gitlab.com) et [GitHub](github.com)